import 'dart:async';

import 'package:flutter/material.dart';
import 'package:grad_app3/models/user.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/services/authentication_services/auth.dart';
import 'package:grad_app3/utils/ui/alert_helper.dart';
import 'package:grad_app3/utils/ui/textfield_helper.dart';

class Signup extends StatefulWidget {
  final Function toggleView;

  Signup({this.toggleView});

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final AuthService _auth = AuthService();
  final _formkey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  //Text Field State
  TextEditingController email = TextEditingController();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController rePassword = TextEditingController();
  String error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
              ),
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.03),
                child: Text(
                  'Sign up',
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Form(
                key: _formkey,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      getWidgetWithPadding(
                        TextFieldHelper.shared.createTextField(
                            controller: email,
                            context: context,
                            labelText: 'Email',
                            keyboardType: TextInputType.emailAddress,
                            prefixIcon:
                                Icon(Icons.email_outlined, color: Color(0xffcc7722)),
                            inputBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).primaryColor),
                            ),
                            validator: (val) =>
                                val.isEmpty ? 'Enter an email' : null),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      getWidgetWithPadding(
                        TextFieldHelper.shared.createTextField(
                            controller: firstName,
                            context: context,
                            labelText: 'First Name',
                            keyboardType: TextInputType.emailAddress,
                            prefixIcon:
                                Icon(Icons.person_outline, color: Color(0xffcc7722)),
                            inputBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).primaryColor),
                            ),
                            validator: (val) =>
                                val.isEmpty ? 'Enter your first name' : null),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      getWidgetWithPadding(
                        TextFieldHelper.shared.createTextField(
                            controller: lastName,
                            context: context,
                            labelText: 'Last Name',
                            keyboardType: TextInputType.emailAddress,
                            prefixIcon:
                                Icon(Icons.person_outline, color: Color(0xffcc7722)),
                            inputBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).primaryColor),
                            ),
                            validator: (val) =>
                                val.isEmpty ? 'Enter your last name' : null),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      getWidgetWithPadding(
                        TextFieldHelper.shared.createTextField(
                            obscureText: true,
                            controller: password,
                            context: context,
                            labelText: 'Password',
                            keyboardType: TextInputType.emailAddress,
                            prefixIcon: Icon(Icons.lock_open_sharp,
                                color: Color(0xffcc7722)),
                            inputBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).primaryColor),
                            ),
                            validator: (val) => val.length < 6
                                ? 'Password must be 6 or more characters'
                                : null),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      getWidgetWithPadding(
                        TextFieldHelper.shared.createTextField(
                            obscureText: true,
                            controller: rePassword,
                            context: context,
                            labelText: 'Re-enter Password',
                            keyboardType: TextInputType.emailAddress,
                            prefixIcon: Icon(Icons.lock_open_sharp,
                                color: Color(0xffcc7722)),
                            inputBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).primaryColor),
                            ),
                            validator: (val) => val.length < 6
                                ? 'Password must be 6 or more characters'
                                : null),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold),
                          ),
                          onPressed: () async {
                            if (_formkey.currentState.validate()) {
                              User myUser = User(
                                  uniEmail: email.text,
                                  password: password.text,
                                  firstName: firstName.text,
                                  lastName: lastName.text);
                              bool result = await _auth
                                  .registerWithEmailAndPassword(myUser);
                              if (result == null || !result) {
                                AlertHelper.shared.showAlertDialog(
                                    message: 'please supply a valid email',
                                    context: context);
                              } else {
                                _showSuccessSnackBar();
                                Timer(Duration(milliseconds: 1000), () {
                                  // widget.toggleView.call();
                                  Navigator.of(context).pushAndRemoveUntil(//todo commit this if you dont want to do silent login
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()),
                                      (route) => false);
                                });
                              }
                            }
                          },
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.015,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Text(
                          "Back",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          widget.toggleView();
                        },
                        color: Theme.of(context).primaryColor,
                      ),
                    ),SizedBox(height: MediaQuery.of(context).size.height * 0.05,)
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  void _showSuccessSnackBar() {
    final snackBar = SnackBar(
      content: Container(
        height: 70.0,
        child: Center(
          child: Text(
            'You\'ve registered Successfully',
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
      duration: Duration(seconds: 2),
      backgroundColor: Color(0xff5CB45C),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}


