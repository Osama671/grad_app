import 'package:flutter/material.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/services/authentication_services/auth.dart';
import 'package:grad_app3/utils/ui/alert_helper.dart';
import 'package:grad_app3/utils/ui/navigator_footer.dart';
import 'package:grad_app3/utils/ui/textfield_helper.dart';

class Login extends StatefulWidget {
  final Function toggleView;

  Login({this.toggleView});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final AuthService _auth = AuthService();

  //Text Field State
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  var _obscurePasswordText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.1,
          ),
          Text(
            'Welcome to the PSUT Club Lounge!',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColor,
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.02,
          ),
          Text(
            'Sign in to your account',
            style: TextStyle(
              fontSize: 15,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Image(
            image: AssetImage('assets/images/PSUT_logo.png'),
            width: MediaQuery.of(context).size.width * 0.35,
          ),
          // SizedBox(
          //   height: MediaQuery.of(context).size.height * 0,
          // ),
          getWidgetWithPadding(
              TextFieldHelper.shared.createTextField(
                  controller: email,
                  context: context,
                  hint: 'Email',
                  labelText: 'Email',
                  keyboardType: TextInputType.emailAddress,
                  prefixIcon: Icon(Icons.email_outlined, color: Color(0xffcc7722)),
                  inputBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Theme.of(context).primaryColor),
                  ),
                  onChanged: (val) {
                    val = email.text.trim();
                  },
                  validator: (val) => val.isEmpty ? 'Enter an email' : null),
              topPadding: 10),
          getWidgetWithPadding(
            TextFieldHelper.shared.createTextField(
              controller: password,
              context: context,
              hint: 'Password',
              keyboardType: TextInputType.visiblePassword,
              prefixIcon: Icon(
                Icons.lock_outline,
                color: Color(0xffcc7722),
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    _obscurePasswordText = !_obscurePasswordText;
                  });
                },
                icon: Icon(
                  _obscurePasswordText
                      ? Icons.visibility
                      : Icons.visibility_off,
                  color: Color(0xffcc7722),
                ),
              ),
              onChanged: (val) {
                val = password.text;
              },
              obscureText: _obscurePasswordText,
              inputBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              labelText: 'Password',
              validator: (val) =>
                  val.length < 6 ? 'Enter a password 6+ char long' : null,
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 40, right: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  value: true,
                  onChanged: (val) {},
                  activeColor: Color(0xffcc7722),
                ),
                Text(
                  "Remember me?",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 14),
                ),
                Expanded(
                  child: InkWell(
                    child: Text(
                      'Forget Password',
                      textAlign: TextAlign.end,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 13,
                      ),
                    ),
                    onTap: () {},
                  ),
                )
              ],
            ),
          ),
          NavigatorFooter(
            context: context,
            title: 'Sign In',
            isCenter: true,
            isPadding: false,
            isActive: true,
            onTap: () async {
              print("Email: " + email.text + " Password: " + password.text);
              // shift + F6
              if (_valid()) {
                bool result = await _auth.signInWithEmailAndPassword(
                    email.text.trim(), password.text.trim());
                if (result == null || !result) {
                  AlertHelper.shared.showAlertDialog(
                      message: 'Could not sign in with those credentials',
                      context: context);
                } else {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => HomePage()),
                      (route) => false);
                }
              }
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Don't have an account? ",
                style: TextStyle(color: Colors.grey),
              ),
              FlatButton(
                child: Text(
                  "Sign up here",
                  style: TextStyle(
                      color: Colors.lightBlue,
                      decoration: TextDecoration.underline),
                ),
                onPressed: () {
                  widget.toggleView();
                },
              )
            ],
          ),
        ],
      ),
    ));
  }

  bool _valid() {
    print(email);
    return (email != null && email.text.isNotEmpty) &&
        (password != null && password.text.isNotEmpty);
  }
}
