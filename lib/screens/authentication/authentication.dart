import 'package:flutter/material.dart';
import 'package:grad_app3/screens/authentication/login.dart';
import 'package:grad_app3/screens/authentication/signup.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> { // state manegement
  bool showSignIn = true;
  void toggleView() {
    setState(() => showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    if ( showSignIn) {
      return Login(toggleView: toggleView);
    } else {
      return Signup(toggleView: toggleView);
    }
  }

}
