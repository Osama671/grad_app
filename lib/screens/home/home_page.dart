import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/calender.dart';
import 'package:grad_app3/Widgets/findclubs.dart';
import 'package:grad_app3/Widgets/manageclubs.dart';
import 'package:grad_app3/Widgets/manageclubs_createclub.dart';
import 'package:grad_app3/Widgets/myclubs.dart';
import 'package:grad_app3/Widgets/navigation.dart';
import 'package:grad_app3/Widgets/news.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'package:grad_app3/screens/wrapper.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';
import 'package:grad_app3/services/authentication_services/auth.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    final double _widthSize = MediaQuery.of(context).size.width * 0.10;
    final double _heightSize = MediaQuery.of(context).size.width * 0.075;
    return Scaffold(
      backgroundColor: Color(0xffcacfcc),
      bottomNavigationBar: bottomNavigationBar(),
      appBar: AppBar(
        leading: Padding(
            padding: EdgeInsets.all(6),
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => Profile()),
                );
              },
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: ClipRRect(
                  child:
                      Image.asset('assets/images/profilepic_placeholder.jpg'),
                  borderRadius: BorderRadius.circular(50),
                ),
              ),
            )),
        title: Text("Welcome, " +
            CurrentSession.shared.user.firstName +
            " " +
            CurrentSession.shared.user.lastName),
      ),
      drawer: Navigation(),
      //Imported class
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.045,
            ),
            Column(
              children: [
                SizedBox(
                  height: 700,
                  width: double.infinity,
                  child: GridView.count(
                    childAspectRatio: _widthSize / _heightSize,
                    padding: EdgeInsets.fromLTRB(70, 60, 60, 60),
                    crossAxisCount: 2,
                    crossAxisSpacing: 15,
                    mainAxisSpacing: 20,
                    children: <Widget>[
                      Stack(children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(17),
                              ),
                              color: Colors.white),
                        ),
                        Positioned(
                          left: 35,
                          top: 15,
                          child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => FindClubs()));
                              },
                              icon: Image.asset(
                                  'assets/icons/icon_findclubs2.png')),
                        ),
                        Positioned(
                          top: 65,
                          left: 27,
                          child: Text(
                            "Find Clubs",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                      Stack(children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(17),
                              ),
                              color: Colors.white),
                        ),
                        Positioned(
                          left: 37,
                          top: 15,
                          child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => MyClubs()));
                              },
                              icon: Image.asset(
                                  'assets/icons/icon_manageclubs.png')),
                        ),
                        Positioned(
                          top: 65,
                          left: 17,
                          child: Text(
                            "Manage Clubs",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                      Stack(children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(17),
                              ),
                              color: Colors.white),
                        ),
                        Positioned(
                          left: 38,
                          top: 15,
                          child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => ManageClubs()));
                              },
                              icon: Image.asset(
                                  'assets/icons/icon_myclubs2.png')),
                        ),
                        Positioned(
                          top: 65,
                          left: 33,
                          child: Text(
                            "My Clubs",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                      Stack(children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(17),
                              ),
                              color: Colors.white),
                        ),
                        Positioned(
                          left: 35,
                          top: 15,
                          child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => Calender()));
                              },
                              icon: Image.asset(
                                  'assets/icons/icon_calendar.png')),
                        ),
                        Positioned(
                          top: 65,
                          left: 31,
                          child: Text(
                            "Calendar",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                      Stack(children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(17),
                              ),
                              color: Colors.white),
                        ),
                        Positioned(
                          left: 35,
                          top: 15,
                          child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => News()));
                              },
                              icon: Image.asset(
                                  'assets/icons/icon_newspaper.png')),
                        ),
                        Positioned(
                          top: 65,
                          left: 42,
                          child: Text(
                            "News",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                      Stack(children: [
                        Container(
                          height: 100,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(17),
                              ),
                              color: Colors.white),
                        ),
                        Positioned(
                          left: 38,
                          top: 15,
                          child: IconButton(
                              onPressed: () async {
                                await _auth.signOut();
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Wrapper()),
                                    (route) => false);
                              },
                              icon:
                                  Image.asset('assets/icons/icon_logout.png')),
                        ),
                        Positioned(
                          top: 65,
                          left: 38,
                          child: Text(
                            "Logout",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ]),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
