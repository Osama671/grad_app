import 'package:flutter/material.dart';
import 'package:grad_app3/screens/authentication/authentication.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (CurrentSession.shared.user == null) {
      return Authenticate();
    } else {
      return HomePage();
    }
  }
}
