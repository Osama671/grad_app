import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';
import 'package:grad_app3/utils/ui/textfield_helper.dart';
import './navigation.dart';

class ProfileEdit extends StatefulWidget {
  @override
  _ProfileEditState createState() => _ProfileEditState();
}
// Replace with CurrentSession information
class _ProfileEditState extends State<ProfileEdit> {
  TextEditingController _dateOfBirthController =
      TextEditingController(text: "25/1/1997");
  TextEditingController _mobileNumberController =
      TextEditingController(text: "0794121512");
  TextEditingController _emailController =
      TextEditingController(text: CurrentSession.shared.user.uniEmail);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => Profile()),
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            Padding(
              padding: EdgeInsets.only(left: 25),
              child: Text(
                "Edit Profile",
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            InkWell(
              child: Container(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.07,
                    0,
                    MediaQuery.of(context).size.width * 0.07,
                    0),
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.5,
                child: Image(
                  image: AssetImage('assets/images/profilepic_placeholder.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
              onTap: () {}, // Allows user to change profile picture
            ),
            Container(
              margin: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.07,
                  MediaQuery.of(context).size.height * 0.1,
                  MediaQuery.of(context).size.width * 0.07,
                  0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Date of Birth",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  TextField(
                    controller: _dateOfBirthController,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                  Text(
                    "Mobile Number",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  TextField(
                    controller: _mobileNumberController,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                  Text(
                    "E-mail",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  TextField(
                    controller: _emailController,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.07,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.only(left: 7)),
                Container(
                  height: MediaQuery.of(context).size.height * 0.06,
                  width: MediaQuery.of(context).size.width * 0.40,
                  child: RaisedButton(
                      color: Colors.blue,
                      child: Text(
                        "Save Changes",
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      onPressed: () {}),
                ),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            )
          ],
        ),
      ),
    );
  }
}

/*getWidgetWithPadding(TextFieldHelper.shared.createTextField(
//labelText: CurrentSession.shared.user.mobileNumber.toString(),
context: context,
hint: 'Mobile Number',
keyboardType: TextInputType.emailAddress,
controller: _dateOfBirthController,
inputBorder: OutlineInputBorder(
borderSide: BorderSide(color: Colors.blue),
),
)),*/
