import 'dart:async';

import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/manageclubs_createclub.dart';
import 'package:grad_app3/Widgets/manageclubs_editclubs.dart';
import 'package:grad_app3/models/club.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/services/club_services/club_services.dart';
import 'package:grad_app3/utils/ui/navigator_footer.dart';

import 'navigation.dart';

class MyClubs extends StatefulWidget {
  @override
  _MyClubsState createState() => _MyClubsState();
}

class _MyClubsState extends State<MyClubs> {
  List<Club> myClubs = [];
  Club editClub;

  @override
  void initState() {
    super.initState();
    _getMyClubs();
  }

  void _getMyClubs() async {
    myClubs = await DatabaseClubService().getMyClubs();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomNavigationBar(),
      appBar: AppBar(
        title: Text(
          "Manage Clubs",
          style: TextStyle(fontSize: 20),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
        ),
      ),
      body: myClubs.length == 0
          ? Center(
              child: Column(
                children: [
                  Text('You don\'t have any club'),
                  NavigatorFooter(
                    // custom button
                    title: 'Create One',
                    context: context,
                    isCenter: true,
                    isPadding: true,
                    isActive: true,
                    onTap: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => CreateClubs()));
                    },
                  ),
                ],
              ),
            )
          : SingleChildScrollView(
              physics: ScrollPhysics(),
              child: Column(
                children: [
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
                          print(editClub.toString() + "123TEST TEST TEST123");
                          editClub = myClubs[index];
                          createAlertDialog(context, editClub, index);

                          print(editClub.title.toString() +
                              "789TEST TEST TEST789");
                          //setState(() {
                          //  myClubs =  [];
                          //});
                          Timer(Duration(milliseconds: 1000), () {
                            _getMyClubs();
                          });
                        },
                        child: clubItem(myClubs[index], context),
                      );
                    },
                    itemCount: myClubs.length,
                  ),
                ],
              ),
            ),
    );
  }

/* Widget clubItem(Club myClub) {
    return Container(
      height: 250,
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Column(
          children: [
            Image.asset('assets/images/PSUT_Campus.jpg'),
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Club Name : ' + myClub.title,
                    style: TextStyle(color: Colors.red),
                  ),
                  Text('Max limit : ' + myClub.noOfPeople,
                      style: TextStyle(color: Colors.orange)),
                  Text('Weekly Meeting time: ' + myClub.meetingTime,
                      style: TextStyle(color: Colors.blue)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}*/

  Widget clubItem(Club myClub, BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.50,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        elevation: 5,
        margin: EdgeInsets.all(20),
        child: Column(children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.01,
          ),
          Image(
            image: AssetImage('assets/images/PSUT_Campus.jpg'),
          ),
          Container(
            alignment: Alignment.center,
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
            child: Text(
              myClub.title, //Club Title
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.025),
          ),
          Container(
            padding:
                EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "People: ",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      myClub.noOfPeople,
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Row(
                  children: [
                    Text(
                      "Location: ",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      myClub.location,
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Row(
                  children: [
                    Text(
                      "Date/Time: ", //Time
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      myClub.meetingTime,
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  createAlertDialog(BuildContext context, Club myClub, var index) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            elevation: 3.0,
            title: Container(
              child: Column(
                children: [
                  Text("Choose your action"),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                  ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width * 0.4,
                      child: RaisedButton(
                          child:
                              Text("Edit Club", style: TextStyle(fontSize: 18)),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ManageClubsEdit(myClub: myClubs[index]),
                              ),
                            );
                          })),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width * 0.4,
                      child: RaisedButton(
                          child: Text(
                            "Events",
                            style: TextStyle(fontSize: 18),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                            AlertDialogEvent(context, myClub, index);
                          })),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width * 0.4,
                    child: RaisedButton(
                      color: Colors.lightBlue,
                      child: Text("News", style: TextStyle(fontSize: 18)),
                      onPressed: () {
                        Navigator.of(context).pop();
                        AlertDialogNews(context, myClub, index);
                      },
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width * 0.4,
                    child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child:
                            Text("Delete club", style: TextStyle(fontSize: 18)),
                        onPressed: () {
                          DatabaseClubService().deleteClub(myClubs[index]);
                          Navigator.of(context, rootNavigator: true).pop();
                        }),
                  ),
                ],
              ),
            ),
          );
        });
  }

  AlertDialogEvent(BuildContext context, Club myClub, var index) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            elevation: 3.0,
            title: Container(
              child: Column(
                children: [
                  Text("Choose your action"),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                  ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width * 0.4,
                      child: RaisedButton(
                          child: Text(
                            "Add Event",
                            style: TextStyle(fontSize: 18),
                          ),
                          onPressed: () {})),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width * 0.4,
                    child: RaisedButton(
                        color: Colors.lightBlue,
                        child: Text("View Event History",
                            style: TextStyle(fontSize: 18)),
                        onPressed: () {}),
                  ),
                ],
              ),
            ),
          );
        });
  }

  AlertDialogNews(BuildContext context, Club myClub, var index) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            elevation: 3.0,
            title: Container(
              child: Column(
                children: [
                  Text("Choose your action"),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.015),
                  ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width * 0.4,
                      child: RaisedButton(
                          child: Text(
                            "Add News",
                            style: TextStyle(fontSize: 18),
                          ),
                          onPressed: () {})),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width * 0.4,
                    child: RaisedButton(
                        color: Colors.lightBlue,
                        child: Text("View News History",
                            style: TextStyle(fontSize: 18)),
                        onPressed: () {}),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
