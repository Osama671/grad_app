import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/findclubs.dart';
import 'package:grad_app3/models/club.dart';
import 'navigation.dart';

class ClubsMoreInfo extends StatefulWidget {
  Club findClubResult;

  ClubsMoreInfo({Key key, this.findClubResult}) : super(key: key);

  @override
  _ClubsMoreInfoState createState() => _ClubsMoreInfoState();
}

class _ClubsMoreInfoState extends State<ClubsMoreInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomNavigationBar(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => FindClubs()),
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.0,
            ),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.30,
              child: Image(
                fit: BoxFit.fill,
                image: AssetImage('assets/images/PSUT_Campus.jpg'),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.04,
            ),
            Container(
              alignment: Alignment.center,
              width: double.infinity,
              child: Text(
                widget.findClubResult.title,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(
                  MediaQuery.of(context).size.width * 0.05,
                  MediaQuery.of(context).size.height * 0.02,
                  MediaQuery.of(context).size.width * 0.05,
                  0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Row(
                    children: [
                      Text(
                        "People: ",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                      Text(widget.findClubResult.title,
                          style: TextStyle(fontSize: 20))
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    children: [
                      Text(
                        "Location: ",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                      Text("Amman", style: TextStyle(fontSize: 20))
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    children: [
                      Text(
                        "Date/Time: ",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                      Text("3:00 Thursday", style: TextStyle(fontSize: 20))
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Center(
                    child: ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width * 0.64,
                      buttonColor: Colors.grey,
                      height: MediaQuery.of(context).size.height * 0.07,
                      child: RaisedButton(
                          child: Text(
                            "View Event/News History",
                            style: TextStyle(fontSize: 20),
                          ),
                          onPressed: () {}),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Center(
                    child: ButtonTheme(
                      buttonColor: Colors.grey,
                      minWidth: MediaQuery.of(context).size.width * 0.64,
                      height: MediaQuery.of(context).size.height * 0.07,
                      child: RaisedButton(
                          child: Text(
                            "View Participants list",
                            style: TextStyle(fontSize: 20),
                          ),
                          onPressed: () {}),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                  Center(
                    child: ButtonTheme(
                      buttonColor: Colors.grey,
                      minWidth: MediaQuery.of(context).size.width * 0.64,
                      height: MediaQuery.of(context).size.height * 0.07,
                      child: RaisedButton(
                          child: Text(
                            "Join Club",
                            style: TextStyle(fontSize: 20),
                          ),
                          onPressed: () {}),
                    ),
                  ),SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
