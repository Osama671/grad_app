import 'package:flutter/material.dart';
import 'navigation.dart';

class MyClubs2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "My Clubs",
        ),
      ),
      drawer: Navigation(),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Club 1:",
                style: TextStyle(fontSize: 32),
              ),
            ),
          ),
          /* Text(
            "My Clubs",
            style: TextStyle(fontSize: 18),
          ), */
          Card(
            elevation: 5,
            margin: EdgeInsets.fromLTRB(0, 35, 0, 0),
            child: Column(children: <Widget>[
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                child: Text(
                  "This is a placeholder for the club's title",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                color: Colors.purple,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / 1.25,
                height: 150,
                child: Text("CLUB IMAGE PLACEHOLDER"),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                child: Row(
                  children: [
                    Text(
                      "People: 5/20",
                      style: TextStyle(fontSize: 18),
                    ),
                    Checkbox(
                        value: false,
                        onChanged:
                            null), //Placeholder for an icon representing people
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                margin: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                alignment: Alignment.centerLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                      child: Text(
                        "Location: Room 303 IT",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Text(
                      "Date/Time: Thursday, 3:00 -> 4:00",
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ),
            ]),
          ),
          Padding(
            padding: EdgeInsets.only(top: 40),
          ),
        ],
      ),
    );
  }
}
