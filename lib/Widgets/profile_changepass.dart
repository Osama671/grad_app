import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'navigation.dart';

class ProfileChangePassword extends StatefulWidget {
  @override
  _ProfileChangePasswordState createState() => _ProfileChangePasswordState();
}

class _ProfileChangePasswordState extends State<ProfileChangePassword> {
  TextEditingController _currentPasswordController = TextEditingController();
  TextEditingController _newPasswordController = TextEditingController();
  TextEditingController _retypeNewPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => Profile()),
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(MediaQuery.of(context).size.height * 0.03,
              50, MediaQuery.of(context).size.height * 0.03, 0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.1),
                  child: Text(
                    "Change Password",
                    style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
                  ),
                ),
                Text(
                  "Current Password",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                TextField(
                  controller: _currentPasswordController,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                Text(
                  "New Password",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                TextField(
                  controller: _newPasswordController,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                Text(
                  "Retype password",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                TextField(
                  controller: _retypeNewPasswordController,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.07,
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.40,
                    child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          "Save Password",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        onPressed: () {}),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
