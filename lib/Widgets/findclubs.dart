import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/clubsmoreinfo.dart';
import 'package:grad_app3/Widgets/manageclubs_createclub.dart';
import 'package:grad_app3/models/club.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/utils/ui/navigator_footer.dart';
import './navigation.dart';
import 'package:grad_app3/services/club_services/club_services.dart';

class FindClubs extends StatefulWidget {
  @override
  _FindClubsState createState() => _FindClubsState();
}

class _FindClubsState extends State<FindClubs> {
  List<Club> allClubs = [];

  @override
  void initState() {
    super.initState();
    _getAllClubs();
  }

  void _getAllClubs() async {
    allClubs = await DatabaseClubService().getAllClubs();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomNavigationBar(),
      // Prevents bottom overflowing of clubs results.
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
        ),
      ),

      body: allClubs.length == 0
          ? Center(
              child: SizedBox(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.25,
                child: Column(
                  children: [
                    Text(
                      "No clubs available",
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      "Please refresh the page or make your own club!",
                      style: TextStyle(fontSize: 18),
                    ),
                    NavigatorFooter(
                      // custom button
                      title: 'Create a new club!',
                      context: context,
                      isCenter: true,
                      isPadding: true,
                      isActive: true,
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => CreateClubs()));
                      },
                    ),
                  ],
                ),
              ),
            )
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Card(
                    elevation: 4.0,
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width / 16, 0, 0, 0),
                          child: Text(
                            "Name",
                            style: TextStyle(color: Colors.grey, fontSize: 24),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width / 8, 0, 0, 0),
                          child: Text(
                            "People",
                            style: TextStyle(color: Colors.grey, fontSize: 24),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(
                              MediaQuery.of(context).size.width / 8, 0, 0, 0),
                          child: Text(
                            "Date",
                            style: TextStyle(color: Colors.grey, fontSize: 24),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        MediaQuery.of(context).size.width / 32,
                        MediaQuery.of(context).size.height / 24,
                        0,
                        0),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Club Results:",
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
                    ),
                  ),
                  ListView.builder(
                    itemCount: allClubs.length,
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {},
                        child: clubItem(allClubs[index], context),
                      );
                    },
                  ),
                ],
              ),
            ),
    );
  }
}

Widget clubItem(Club myClub, BuildContext context) {
  return GestureDetector(
    onTap: () {
      print("Tapped!");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ClubsMoreInfo(findClubResult: myClub),
        ),
      );
      // Navigator.of(context).pushReplacement(
      // MaterialPageRoute(builder: (context) => ClubsMoreInfo()));
    },
    child: SizedBox(
      height: MediaQuery.of(context).size.height * 0.50,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        elevation: 5,
        margin: EdgeInsets.fromLTRB(20, 35, 20, 0),
        child: Column(children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.01,
          ),
          Image(
            image: AssetImage('assets/images/PSUT_Campus.jpg'),
          ),
          Container(
            alignment: Alignment.center,
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
            child: Text(
              myClub.title, //Club Title
              style: TextStyle(
                fontSize: 24,
              ),
            ),
          ),

          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025),
          ),
          Container(
            padding:
                EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "People: ",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      myClub.noOfPeople,
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Row(
                  children: [
                    Text(
                      "Location: ",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      myClub.location,
                      style: TextStyle(fontSize: 18),
                    )
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Row(
                  children: [
                    Text(
                      "Date/Time: ", //Time
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      myClub.meetingTime,
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]),
      ),
    ),
  );
}
