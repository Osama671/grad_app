import 'package:flutter/material.dart';
import 'navigation.dart';

class NewsNone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "My Clubs",
        ),
      ),
      drawer: Navigation(),
      body: SizedBox.expand(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "There are no news available as you are not in a club.",
              style: TextStyle(fontSize: 16),
            ),
            RaisedButton(
              color: Colors.blue,
              child: Text(
                "Join a club now!",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
              onPressed: null,
            ),
          ],
        ),
      ),
    );
  }
}
