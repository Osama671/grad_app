import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/profile_changepass.dart';
import 'package:grad_app3/Widgets/profile_edit.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';
import 'navigation.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomNavigationBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 350,
                  child: Image(
                    image:
                        AssetImage('assets/images/profilepic_placeholder.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                  bottom: 15,
                  left: 15,
                  child: Text(
                    CurrentSession.shared.user.firstName +
                        " " +
                        CurrentSession.shared.user.lastName,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Date of Birth",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  Text(
                    "25/1/1997",
                    style: TextStyle(fontSize: 16),
                  ),
                  Divider(
                    thickness: 2,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  Text(
                    "Mobile Number",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  Text(
                    "0791519241",
                    style: TextStyle(fontSize: 16),
                  ),
                  Divider(
                    thickness: 2,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  Text(
                    "E-mail",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  Text(
                    CurrentSession.shared.user.uniEmail,
                    style: TextStyle(fontSize: 16),
                  ),
                  Divider(
                    thickness: 2,
                  ),
                ],
              ),
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(height: MediaQuery.of(context).size.height * 0.06, width: MediaQuery.of(context).size.width * 0.30,
                    child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          "Edit Profile",
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        onPressed: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                                builder: (context) => ProfileEdit()),
                          );
                        }),
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(40, 60, 40, 60)),
                  Container(height: MediaQuery.of(context).size.height * 0.06, width: MediaQuery.of(context).size.width * 0.30,
                    child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child: Text(
                          "Change Password",textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        onPressed: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                                builder: (context) => ProfileChangePassword()),
                          );
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
