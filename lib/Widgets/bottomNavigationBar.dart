import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'package:grad_app3/screens/home/home_page.dart';

class bottomNavigationBar extends StatefulWidget {
  @override
  _bottomNavigationBarState createState() => _bottomNavigationBarState();
}

class _bottomNavigationBarState extends State<bottomNavigationBar> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      iconSize: 30,
      selectedFontSize: 14,
      currentIndex: _currentIndex,
      items: [
        BottomNavigationBarItem(
          icon: IconButton(
            icon: Image.asset('assets/icons/icon_home.png'), onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                  builder: (context) => HomePage()),);
          },),
          title: Text(
            'Home', style: TextStyle(color: Colors.black, fontSize: 16),),
          backgroundColor: Colors.blue,
        ),
        BottomNavigationBarItem(
          icon: IconButton(
            icon: Image.asset('assets/icons/icon_profile.png'), onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                  builder: (context) => Profile()),);
          },),
          title: Text('My Profile', style: TextStyle(color: Colors.black),),
          backgroundColor: Colors.red,
        ),
      ],
      onTap: (index) {
        setState(() {
          _currentIndex = index;
        });
      },
    );
  }
}
