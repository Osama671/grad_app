import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/bottomNavigationBar.dart';
import 'package:grad_app3/Widgets/myclubs.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'package:grad_app3/models/club.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/services/club_services/club_services.dart';
import 'package:grad_app3/utils/ui/alert_helper.dart';
import 'package:grad_app3/utils/ui/navigator_footer.dart';
import 'package:grad_app3/utils/ui/textfield_helper.dart';

import './navigation.dart';

class ManageClubsEdit extends StatefulWidget {
  Club myClub;

  ManageClubsEdit({Key key, this.myClub}) : super(key: key);

  @override
  _ManageClubsEditState createState() => _ManageClubsEditState();
}

class _ManageClubsEditState extends State<ManageClubsEdit> {
  int _currentIndex = 0;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _noOfPeopleController = TextEditingController();
  TextEditingController _locationClubController = TextEditingController();
  TextEditingController _meetingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomNavigationBar(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => MyClubs()),
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(left: 12),
              child: Text(
                "Edit Club",
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            GestureDetector(
              // When you tap on the default image, users can change to the image of their choice.
              onTap: () {
                print("Hello");
              },
              child: Container(
                height: MediaQuery.of(context).size.height * 0.30,
                width: MediaQuery.of(context).size.width * 0.85,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/PSUT_Campus.jpg'),
                      fit: BoxFit.fill),
                ),
              ),
            ),
            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
              onTap: () {
                print(widget.myClub.title);
              },
              labelText: "Club Name",
              context: context,
              hint: 'Club Name',
              keyboardType: TextInputType.emailAddress,
              controller: _nameController =
                  new TextEditingController(text: widget.myClub.title),
              inputBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            )),

            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
              labelText: "People Limit",
              context: context,
              hint: 'People limit',
              keyboardType: TextInputType.phone,
              controller: _noOfPeopleController =
                  new TextEditingController(text: widget.myClub.noOfPeople),
              inputBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            )),

            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
              labelText: "Location",
              context: context,
              hint: 'Location',
              keyboardType: TextInputType.emailAddress,
              inputBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              controller: _locationClubController =
                  new TextEditingController(text: widget.myClub.location),
            )),

            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
              labelText: "Meeting Time",
              context: context,
              hint: 'Weekly Meeting Time',
              keyboardType: TextInputType.emailAddress,
              controller: _meetingController =
                  new TextEditingController(text: widget.myClub.meetingTime),
              inputBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            )),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),

            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.1),
                ),
                ButtonTheme(
                  minWidth: 141,
                  child: RaisedButton(
                      color: Theme.of(context).primaryColor,
                      child: Text(
                        "Confirm Changes",
                        style: TextStyle(fontSize: 16,color: Colors.white),
                      ),
                      elevation: 3.2,
                      onPressed: () {
                        Club club = Club(
                            title: _nameController.text,
                            location: _locationClubController.text,
                            meetingTime: _meetingController.text,
                            noOfPeople: _noOfPeopleController.text);
                        DatabaseClubService().editClub(club, widget.myClub);
                        print("WORKED TEST TEST WORKED");
                      }),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.08),
                ),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            )

            //Plus Sign
          ],
        ),
      ),
    );
  }
}
