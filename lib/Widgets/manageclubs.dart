import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/manageclubs_createclub.dart';
import 'package:grad_app3/Widgets/manageclubs_editclubs.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import './navigation.dart';

class ManageClubs extends StatefulWidget {
  @override
  _ManageClubsState createState() => _ManageClubsState();
}

class _ManageClubsState extends State<ManageClubs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
        ),
      ),

      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Club 1:",
                style: TextStyle(fontSize: 32),
              ),
            ),
          ),
          Card(
            elevation: 5,
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(children: <Widget>[
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                child: Text(
                  "This is a placeholder for the club's title",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                color: Colors.purple,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / 1.25,
                height: 150,
                child: Text("CLUB IMAGE PLACEHOLDER"),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                child: Row(
                  children: [
                    Text(
                      "People: 5/20",
                      style: TextStyle(fontSize: 18),
                    ),
                    Checkbox(value: false, onChanged: null),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(90, 0, 0, 0),
                      child: RaisedButton(
                        child: Text(
                          "Edit",
                          style: TextStyle(fontSize: 18),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ManageClubsEdit(),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                margin: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width * 0.1, 0, 0, 0),
                alignment: Alignment.centerLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                      child: Text(
                        "Location: Room 303 IT",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Text(
                      "Date/Time: Thursday, 3:00 -> 4:00",
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ),
            ]),
          ),
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 40),
                  child: RaisedButton(
                    child: Text(
                      "Create Club",
                      style: TextStyle(fontSize: 18),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CreateClubs(),
                        ),
                      );
                    },
                  ))
            ],
          )
        ],
      ),
    );
  }
}
