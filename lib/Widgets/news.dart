import 'package:flutter/material.dart';
import 'navigation.dart';

class News extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "News",
        ),
      ),
      drawer: Navigation(),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "11/10/2020:",
              style: TextStyle(fontSize: 24),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
              child: Column(
                children: <Widget>[
                  RichText(
                    text: TextSpan(style: TextStyle(), children: <TextSpan>[
                      TextSpan(
                        text: "Club_name1:",
                        style: TextStyle(
                            color: Colors.green,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                      TextSpan(
                        text: " This is a placeholder for news.",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  RichText(
                    text: TextSpan(style: TextStyle(), children: <TextSpan>[
                      TextSpan(
                        text: "Club_name2:",
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                      TextSpan(
                        text: " This is a placeholder for news.",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  RichText(
                    text: TextSpan(style: TextStyle(), children: <TextSpan>[
                      TextSpan(
                        text: "Club_name3:",
                        style: TextStyle(
                            color: Colors.purple,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                      TextSpan(
                        text: " This is a placeholder for news.",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ]),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "10/10/2020:",
              style: TextStyle(fontSize: 24),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
              child: Column(
                children: <Widget>[
                  RichText(
                    text: TextSpan(style: TextStyle(), children: <TextSpan>[
                      TextSpan(
                        text: "Club_name2:",
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                      TextSpan(
                        text: " This is a placeholder for news.",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  RichText(
                    text: TextSpan(style: TextStyle(), children: <TextSpan>[
                      TextSpan(
                        text: "Club_name3:",
                        style: TextStyle(
                            color: Colors.purple,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                      TextSpan(
                        text: " This is a placeholder for news.",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ]),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  RichText(
                    text: TextSpan(style: TextStyle(), children: <TextSpan>[
                      TextSpan(
                        text: "Club_name1:",
                        style: TextStyle(
                            color: Colors.green,
                            decoration: TextDecoration.underline,
                            fontSize: 16),
                      ),
                      TextSpan(
                        text: " This is a placeholder for news.",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      )
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
