import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/calender.dart';
import 'package:grad_app3/Widgets/findclubs.dart';
import 'package:grad_app3/Widgets/manageclubs.dart';
import 'package:grad_app3/Widgets/myclubs.dart';
import 'package:grad_app3/Widgets/news.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'package:grad_app3/screens/authentication/login.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/screens/wrapper.dart';
import 'package:grad_app3/services/authentication_services/auth.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
            color: Colors.blue[100],
            alignment: Alignment.topCenter,
            height: 180,
            child: Column(
              children: [
                Container(
                  //Profile Image Placeholder
                  alignment: Alignment.center,
                  child: Text("Profile picture placeholder"),
                  margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                  height: 80,
                  width: 100,
                  color: Colors.red,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                  // color: Colors.green,
                  height: 40,
                  width: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        CurrentSession.shared.user!=null?CurrentSession.shared.user.firstName+' '+CurrentSession.shared.user.lastName:'USER',
                        style: TextStyle(fontSize: 24),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "Home",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FindClubs(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "Find Clubs",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MyClubs(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "My Clubs",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ManageClubs(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "Manage Clubs",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Calender(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "Calendar",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => News(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "News",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(),
                ),
              );
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "My Profile",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
          ListTile(
            onTap: () async {
              await _auth.signOut();
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Wrapper()),
                  (route) => false);
            },
            title: Container(
              color: Colors.yellow,
              alignment: Alignment.center,
              child: Text(
                "Log out",
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
