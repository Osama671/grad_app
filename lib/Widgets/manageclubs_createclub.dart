import 'package:flutter/material.dart';
import 'package:grad_app3/Widgets/manageclubs.dart';
import 'package:grad_app3/Widgets/profile.dart';
import 'package:grad_app3/models/club.dart';
import 'package:grad_app3/screens/home/home_page.dart';
import 'package:grad_app3/services/club_services/club_services.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';
import 'package:grad_app3/utils/ui/alert_helper.dart';
import 'package:grad_app3/utils/ui/navigator_footer.dart';
import 'package:grad_app3/utils/ui/textfield_helper.dart';

import './navigation.dart';

class CreateClubs extends StatefulWidget {
  @override
  _CreateClubsState createState() => _CreateClubsState();
}

class _CreateClubsState extends State<CreateClubs> {
  int _currentIndex = 0;
  TextEditingController _locationController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _limitController = TextEditingController();
  TextEditingController _meetingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 30,
        selectedFontSize: 14,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: IconButton(
              icon: Image.asset('assets/icons/icon_home.png'),
              onPressed: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => HomePage()),
                );
              },
            ),
            title: Text(
              'Home',
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
            backgroundColor: Colors.blue,
          ),
          BottomNavigationBarItem(
            icon: IconButton(
              icon: Image.asset('assets/icons/icon_profile.png'),
              onPressed: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => Profile()),
                );
              },
            ),
            title: Text(
              'My Profile',
              style: TextStyle(color: Colors.black),
            ),
            backgroundColor: Colors.red,
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => ManageClubs()),
            );
          },
        ),
      ),
      drawer: Navigation(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(left: 12),
              child: Text(
                "Create a new club!",
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.03,
            ),
            GestureDetector(
              // When you tap on the default image, users can change to the image of their choice.
              onTap: () {
                print("Hello");
              },
              child: Container(
                height: MediaQuery.of(context).size.height * 0.17,
                width: MediaQuery.of(context).size.width * 0.60,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/PSUT_Campus.jpg'),
                      fit: BoxFit.fill),
                ),
              ),
            ),
            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
                context: context,
                hint: 'Club Name',
                labelText: 'Club Name',
                keyboardType: TextInputType.emailAddress,
                controller: _nameController,
                inputBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
                validator: (val) => val.isEmpty ? 'Enter an email' : null)),

            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
                context: context,
                hint: 'Club People Limit',
                labelText: 'Club People Limit',
                keyboardType: TextInputType.phone,
                controller: _limitController,
                inputBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
                validator: (val) => val.isEmpty ? 'Enter an email' : null)),

            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
                context: context,
                hint: 'Location',
                labelText: 'Location',
                keyboardType: TextInputType.emailAddress,
                inputBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
                controller: _locationController,
                validator: (val) => val.isEmpty ? 'Enter an email' : null)),

            getWidgetWithPadding(TextFieldHelper.shared.createTextField(
                context: context,
                hint: 'Weekly Meeting Time',
                labelText: 'Weekly Meeting Time',
                keyboardType: TextInputType.emailAddress,
                controller: _meetingController,
                inputBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
                validator: (val) => val.isEmpty ? 'Enter an email' : null)),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            RawMaterialButton(
              onPressed: () {},
              elevation: 2.0,
              fillColor: Colors.white,
              child: InkWell(
                onTap: () {
                  print("Tapped!");
                  extraHeader(context);
                  setState(() {});
                },
                child: Icon(
                  Icons.add,
                  size: 30.0,
                ),
              ),
              padding: EdgeInsets.all(2.0),
              shape: CircleBorder(),
            ),
            NavigatorFooter(
              title: 'Create Club',
              isActive: true,
              isPadding: false,
              isCenter: true,
              onTap: () async {
                if (_isValid()) {
                  print('anas');
                  Club club = Club(
                      title: _nameController.text,
                      location: _locationController.text,
                      meetingTime: _meetingController.text,
                      ownerID: CurrentSession.shared.user.userID,
                      noOfPeople: _limitController.text);
                  DatabaseClubService().addClub(club);
                  AlertHelper.shared.showAlertDialog(
                      message: 'Your Club Created Successfully',
                      context: context);
                } else {
                  AlertHelper.shared.showAlertDialog(
                      message: 'Missing/Incorrect fields', context: context);
                }
              },
            ), //Plus Sign
          ],
        ),
      ),
    );
  }

  bool _isValid() {
    return (_nameController.text != null && _nameController.text.isNotEmpty) &&
        (_limitController.text != null && _limitController.text.isNotEmpty) &&
        (_locationController.text != null &&
            _locationController.text.isNotEmpty) &&
        (_meetingController.text != null && _meetingController.text.isNotEmpty);
  }
}

Widget extraHeader(BuildContext context) {
  return Column(
    children: [
      (getWidgetWithPadding(
        TextFieldHelper.shared.createTextField(
          context: context,
          inputBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.blue),
          ),
        ),
      )),
      SizedBox(
        height: MediaQuery.of(context).size.height * 0.03,
      ),
      Container(
        margin: EdgeInsets.all(8.0),
        // hack textfield height
        padding: EdgeInsets.only(bottom: 40.0),
        child: TextField(
          maxLines: 10,
          decoration: InputDecoration(
            hintText: "Comment!",
            border: OutlineInputBorder(),
          ),
        ),
      ),
    ],
  );
}
