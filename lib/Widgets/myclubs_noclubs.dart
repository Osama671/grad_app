import 'package:flutter/material.dart';
import 'navigation.dart';

class MyClubsNone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "My Clubs",
        ),
      ),
      drawer: Navigation(),
      body: SizedBox.expand(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "You are not registered in any clubs.",
              style: TextStyle(fontSize: 18),
            ),
            RaisedButton(
              color: Colors.blue,
              child: Text(
                "Search for clubs!",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
              onPressed: null,
            )
          ],
        ),
      ),
    );
  }
}
