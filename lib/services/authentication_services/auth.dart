import 'package:firebase_auth/firebase_auth.dart';
import 'package:grad_app3/models/user.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

import 'database.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //Create user object based on FirebaseUser
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null
        ? User(
            userID: user.uid,
            firstName: user.displayName,
          )
        : null;
  }

  bool _userFromFirebaseUserWithSavedSession(FirebaseUser user, User myUser) {
    if (user != null) {
      myUser.userID = user.uid;
      CurrentSession.shared.user = myUser; //todo commit this if you dont want to do silent login
      return true;
    } else
      return false;
  }

  //Auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
        //.map((FirebaseUser user) => _userFromFirebaseUser(user));
        .map(_userFromFirebaseUser);
  }

  // Sign in Anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toSTring());
      return null;
    }
  }

  //Register with Email and Password
  Future<bool> registerWithEmailAndPassword(User myUser) async {
    try {
      //Firebase built-in method
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: myUser.uniEmail, password: myUser.password);
      FirebaseUser user = result.user;
      //Create a new document for the user with the uid
            await DatabaseAuthService(uid: user.uid).updateUserData(myUser);
          return _userFromFirebaseUserWithSavedSession(user, myUser);
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  //Sign in with Email and Pass
  Future<bool> signInWithEmailAndPassword(String email, String password) async {
    try {
      //Firebase built-in method
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      User myUser =
          await DatabaseAuthService(uid: user.uid).getUserById(user.uid);
      if (myUser != null) {
        myUser.userID=user.uid;
        CurrentSession.shared.user=myUser;
        return true;
      }
      return false;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  //Sign out

  Future signOut() async {
    try {
      CurrentSession.shared.user=null;
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
