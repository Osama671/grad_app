import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:grad_app3/models/user.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

class DatabaseAuthService {
  final String uid;
  DocumentReference doc_ref = Firestore.instance.collection("users").document();

  DatabaseAuthService({this.uid});

  // Collection Reference
  final CollectionReference usersCollection =
      Firestore.instance.collection('users');

  Future<void> updateUserData(User user) async {
    // future + async
     await usersCollection.document(uid).setData({
      'firstName': user.firstName,
      'lastName': user.lastName,
      'email': user.uniEmail,
      // 'dateOfBirth': user.dateOfBirth,
      // 'mobileNumber': user.mobileNumber,
    });
  }

  Future<User> getUserById(String uid) async {
    DocumentSnapshot variable =
        await Firestore.instance.collection('users').document(uid).get();
     User myUser = User.fromJson(variable.data);
    return myUser;

    // DocumentSnapshot docSnap = await doc_ref.get();
    // var doc_id2 = docSnap.reference.documentID;
    // return await usersCollection
    //     .getDocuments()
    //     .then((QuerySnapshot snapshot) async {
    //   return await _getUserFromJson(snapshot);
    }


  Future<User> _getUserFromJson(QuerySnapshot snapshot) async {
    User user;

    /// decleration
    snapshot.documents.forEach((value) {
      print('saved' + value.data.toString());
      User myUser = User.fromJson(value.data);
      myUser.userID = uid;
      CurrentSession.shared.user = myUser;
      user = myUser;
    });
    return user;
  }
}
