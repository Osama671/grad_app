import 'package:firebase_auth/firebase_auth.dart';
import 'package:grad_app3/models/user.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

import 'authentication_services/database.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //Create user object based on FirebaseUser
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null
        ? _getUser(user)
        : null;
  }

  User _userFromFirebaseUserWithSavedSession(FirebaseUser user, User myUser) {
    User result;
    if (user != null) {
      myUser.userID = user.uid;
      result = myUser;
      CurrentSession.shared.user = myUser;
    } else
      result = null;
    return result;
  }

  //Auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
        //.map((FirebaseUser user) => _userFromFirebaseUser(user));
        .map(_userFromFirebaseUser);
  }

  // Sign in Anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toSTring());
      return null;
    }
  }

  //Register with Email and Password
  Future registerWithEmailAndPassword(User myUser) async {
    try {
      //Firebase built-in method
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: myUser.uniEmail, password: myUser.password);
      FirebaseUser user = result.user;
      var isUpdated;
          await DatabaseAuthService(uid: user.uid).updateUserData(myUser);
      if (isUpdated) // isUpdated == true
        return _userFromFirebaseUserWithSavedSession(user, myUser);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Sign in with Email and Pass
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      //Firebase built-in method
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email.trim(), password: password);
      FirebaseUser user = result.user;
      User myUser =
          await DatabaseAuthService(uid: user.uid).getUserById(user.uid);
      if (myUser != null) {
        myUser.userID=user.uid;
        return myUser;
      }
      return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Sign out

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

 User _getUser(FirebaseUser user) {

  }
}
