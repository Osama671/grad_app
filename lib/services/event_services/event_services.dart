import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:grad_app3/models/club.dart';
import 'package:grad_app3/models/events.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

class DatabaseEventService{
  DatabaseEventService();

  final CollectionReference eventsCollection =
  Firestore.instance.collection('events');

  Future<List<Events>> getMyEvents(Club myClub) async {
    return await eventsCollection
        .where('clubname', isEqualTo: myClub.title)
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      return _getEventsFromJson(snapshot);
    });
  }

  List<Events> _getEventsFromJson(QuerySnapshot snapshot) {
    List<Events> EventList = [];
    snapshot.documents.forEach((f) {
      var Event = Events.fromJson(f.data);
      Event.eventID = f.documentID;
      EventList.add(Event);
    });
    return EventList;
  }

  Future addEvent(Events myEvent, Club club) async {
    return await eventsCollection.document().setData(myEvent.toJson());
  }

}