import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:grad_app3/models/club.dart';
import 'package:grad_app3/utils/helpers/current_session.dart';

class DatabaseClubService {
  DatabaseClubService();

  // Collection Reference
  final CollectionReference clubsCollection =
      Firestore.instance.collection('clubs');

  Future<List<Club>> getMyClubs() async {
    return await clubsCollection
        .where('ownerId', isEqualTo: CurrentSession.shared.user.userID)
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      return _getClubFromJson(snapshot);
    });
  }

  Future<List<Club>> getAllClubs() async {
    return await clubsCollection.getDocuments().then((QuerySnapshot snapshot) {
      return _getClubFromJson(snapshot);
    });
  }

  List<Club> _getClubFromJson(QuerySnapshot snapshot) {
    List<Club> clubs = [];
    snapshot.documents.forEach((f) {
      var club = Club.fromJson(f.data);
      club.clubID = f.documentID;
      clubs.add(club);
    });
    return clubs;
  }

  Future addClub(Club club) async {
    return await clubsCollection.document().setData(club.toJson());
  }

  Future deleteClub(Club club) async {
    return await clubsCollection.document(club.clubID).delete();
  }

  Future editClub(Club newClub, Club currentClub) async {
    newClub.ownerID = CurrentSession.shared.user.userID;
    return await clubsCollection
        .document(currentClub.clubID) //currentClub is used to reference the clubID that the user is updating the data on.
        .updateData(newClub.toJson());
  }
}
