import 'dart:convert';

User UserFromJson(String str) => User.fromJson(json.decode(str));

String UserToJson(User data) => json.encode(data.toJson());

class User {
  String userID;
  String firstName;
  String lastName;
  String uniEmail;
  String mobileNumber;
  DateTime dateOfBirth;
  String profileImage;
  String password;
  int createdClubsID;
  int joinedClubsID;

  User(
      {this.userID,
      this.firstName,
      this.lastName,
      this.uniEmail,
      this.mobileNumber,
      this.dateOfBirth,
      this.profileImage,
      this.password,
      this.createdClubsID,
      this.joinedClubsID});

  factory User.fromJson(Map<String, dynamic> json) {
    return User( // {firstName: anas, lastName: rashees, email: anas2@gmail.com}
    lastName: json["lastName"] == null ? '' : json["lastName"],
    firstName: json["firstName"] == null ? 'USER' : json["firstName"],
    uniEmail: json["email"] == null ? 'USER@EXAMLE.COM' : json["email"],
  );
  }

  Map<String, dynamic> toJson() => {
    "lastName": lastName == null ? null : lastName,
    "firstName": firstName == null ? null : firstName,
    "email": uniEmail == null ? null : uniEmail,
  };


}
