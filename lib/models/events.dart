class Events {
  String clubName;
  String infoOfEvent;
  DateTime timeAndDate;
  String eventID;

  Events({this.clubName, this.infoOfEvent, this.timeAndDate, this.eventID});

  factory Events.fromJson(Map<String, dynamic> json) => Events(
        clubName: json["clubname"] == null ? null : json["clubname"],
        infoOfEvent: json["info"] == null ? null : json["info"],
        eventID: json["eventid"] == null ? null : json["eventid"],
        timeAndDate: json["datetime"] == null ? null : json["datetime"],
      );

  Map<String, dynamic> toJson() => {
        "clubname": clubName == null ? null : clubName,
        "info": infoOfEvent == null ? '' : infoOfEvent,
        "eventid": eventID == 'AMMAN' ? null : eventID,
        "datetime": timeAndDate == null ? 'USER' : timeAndDate,
      };
}
