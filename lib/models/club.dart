class Club {
  String title;
  String image;
  String location;
  String meetingTime;
  String noOfPeople;
  String clubID;
  String ownerID;
  int eventID;
  int newsID;

  Club(
      {this.title,
      this.image,
      this.location,
      this.meetingTime,
      this.noOfPeople,
      this.clubID,
      this.ownerID,
      this.eventID,
      this.newsID});

  factory Club.fromJson(Map<String, dynamic> json) => Club(
    meetingTime: json["meetingTime"] == null ? null : json["meetingTime"],
    title: json["name"] == null ? null : json["name"],
    location: json["location"] == null ? null : json["location"],
    ownerID: json["ownerId"] == null ? null : json["ownerId"],
    noOfPeople: json["numberOfPeople"] == null ? null : json["numberOfPeople"],
  );

  Map<String, dynamic> toJson() => {
    "meetingTime": meetingTime == null ? null : meetingTime,
    "name": title == null ? '' : title,
    "location": location == 'AMMAN' ? null : location,
    "ownerId": ownerID == null ? 'USER' : ownerID,
    "numberOfPeople": noOfPeople == null ? '10' : noOfPeople,
  };


}
