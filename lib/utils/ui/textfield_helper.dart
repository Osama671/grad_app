import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:select_form_field/select_form_field.dart';

class TextFieldHelper {
  static var shared = TextFieldHelper();
  int otpLength = 4;
  int pinCodeLength = 4;

  void init() {
  }

  Widget createDisabledField({
    TextEditingController controller,
    String errorText = '',
    String labelText = '',
    String prefixText = '',
    IconData icon,
    BuildContext context,
    String defaultText = '',
  }) {
    controller.text = defaultText;
    return setupDisabledTextField(
        controller: controller,
        errorText: errorText,
        labelText: labelText,
        icon: icon,
        prefixText: prefixText,
        context: context);
  }

  Widget setupDisabledTextField(
      {TextEditingController controller,
      String errorText,
      String labelText,
      IconData icon,
      String prefixText = '',
      String suffixText = '',
      BuildContext context}) {
    const fillColor =
        const Color(0xffefefef); // Second `const` is optional in assignments.

    return new TextField(
      enableInteractiveSelection: false,
      // will disable paste operation
      focusNode: new AlwaysDisabledFocusNode(),
      controller: controller,
      autofocus: false,
      style: getTextStyle(),
      decoration: new InputDecoration(
        filled: true,
        prefixStyle: getTextStyle(),
        prefixText: prefixText,
        suffixText: suffixText,
        labelText: labelText,
        fillColor: fillColor,
      ),
    );
  }

  TextStyle getTextStyle({double height = 1}) =>
      new TextStyle(fontSize: 15, height: height, color: Colors.black);


  Widget createTextField({
    TextInputType keyboardType = TextInputType.text,
    TextEditingController controller,
    Key key,
    String errorText = '',
    String labelText = '',
    String hintText = '',
    IconData icon = Icons.filter_none,
    String prefixText = '',
    String suffixText = '',
    void Function(String) onChanged,
    void Function() onTap,
    void Function() onEditingComplete,
    void Function(String s) onSubmitted,
    FocusNode focusNode,
    bool obscureText = false,
    bool enabled = true,
    bool isRead = false,
    List<FocusNode> allFocusNode,
    ImageIcon prefixIconData,
    ImageIcon suffixIconData,
    List<TextInputFormatter> inputFormatters,
    Widget prefixIcon,
    Widget suffixIcon,
    Gradient borderGradient,
    Gradient iconPrefixGradient,
    Gradient iconSuffixGradient,
    InputBorder inputBorder,
    void Function() onGradientPressed,
    TextInputType textInputType = TextInputType.text,
    TextInputAction inputAction = TextInputAction.done,
    BuildContext context,
    String hint,
    bool enableSuggestions = false,
    double strokeWidth = 2,
    bool autocorrect = false,
    FormFieldValidator<String> validator,
    LinearGradient gradientColor,
    TextCapitalization textCapitalization = TextCapitalization.none,
    EdgeInsetsGeometry contentPadding,
    FloatingLabelBehavior floatingLabelBehavior = FloatingLabelBehavior.auto,
    double height = 1,
  }) {
    if (borderGradient == null) {
      borderGradient = gradientColor;
    }
    if (inputBorder == null) {
      inputBorder = OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
      );
    }

    if (iconPrefixGradient == null) {
      iconPrefixGradient = gradientColor;
    }

    if (iconSuffixGradient == null) {
      iconSuffixGradient = gradientColor;
    }

    return TextFormField(
      textCapitalization: textCapitalization,
      autocorrect: autocorrect,
      readOnly: isRead,
      enableSuggestions: enableSuggestions,
      cursorColor: Theme.of(context).primaryColor,
      keyboardType: keyboardType,
      textInputAction: inputAction,
      controller: controller,
      autofocus: false,
      focusNode: focusNode != null ? focusNode : new FocusNode(),
      onChanged: onChanged,
      validator: validator,
      // onSubmitted: onSubmitted,
      enabled: enabled,
      decoration: InputDecoration(
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        floatingLabelBehavior: floatingLabelBehavior,
        contentPadding: contentPadding,
        prefixText: prefixText,
        suffixText:suffixText,
        labelText: labelText,
        labelStyle: getTextStyle(),
        hintText: hintText,
        errorMaxLines: 3,
        errorText: errorText.isEmpty ? null : errorText,
        prefixStyle: getTextStyle(),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        border: inputBorder,
      ),
      obscureText: obscureText,
      style: getTextStyle(height: height),
      inputFormatters: inputFormatters,
      onTap: onTap,
      onEditingComplete: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (onEditingComplete != null) {
          onEditingComplete();
        }
      },
    );
  }

  Widget createDropDown({
    TextInputType keyboardType = TextInputType.text,
    TextEditingController controller,
    SelectFormFieldType dropDownType = SelectFormFieldType.dropdown,
    String errorText = '',
    String labelText = '',
    String hintText = '',
    String dialogTitle = '',
    String dialogSearchHint = '',
    bool enableSearch = false,
    IconData icon = Icons.filter_none,
    String prefixText = '',
    String suffixText = '',
    void Function(String) onChanged,
    void Function(String) onSaved,
    List<Map<String, dynamic>> items,
    void Function() onTap,
    void Function() onEditingComplete,
    void Function(String s) onSubmitted,
    FocusNode focusNode,
    bool obscureText = false,
    bool enabled = true,
    List<FocusNode> allFocusNode,
    ImageIcon prefixIconData,
    ImageIcon suffixIconData,
    List<TextInputFormatter> inputFormatters,
    Widget prefixIcon,
    Widget suffixIcon,
    Gradient borderGradient,
    Gradient iconPrefixGradient,
    Gradient iconSuffixGradient,
    InputBorder inputBorder,
    void Function() onGradientPressed,
    TextInputType textInputType = TextInputType.text,
    TextInputAction inputAction = TextInputAction.done,
    BuildContext context,
    String hint,
    bool enableSuggestions = false,
    double strokeWidth = 2,
    bool autocorrect = false,
    LinearGradient gradientColor,
    TextCapitalization textCapitalization = TextCapitalization.none,
    EdgeInsetsGeometry contentPadding,
    FloatingLabelBehavior floatingLabelBehavior = FloatingLabelBehavior.auto,
    double height = 1,
  }) {
    if (borderGradient == null) {
      borderGradient = gradientColor ;
    }
    if (inputBorder == null) {
      inputBorder = OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
      );
    }

    if (iconPrefixGradient == null) {
      iconPrefixGradient = gradientColor;
    }

    if (iconSuffixGradient == null) {
      iconSuffixGradient = gradientColor;
    }
    return SelectFormField(
      type: dropDownType,
      icon: Icon(icon),
      labelText: 'Shape',
      items: items,
      strutStyle: StrutStyle(height: 1),
      textCapitalization: textCapitalization,
      autocorrect: autocorrect,
      enableSuggestions: enableSuggestions,
      cursorColor: Theme.of(context).primaryColor,
      keyboardType: keyboardType,
      textInputAction: inputAction,
      controller: controller,
      autofocus: false,
      style: getTextStyle(height: height),
      focusNode: focusNode != null ? focusNode : new FocusNode(),
      onChanged: onChanged,
      enabled: enabled,
      decoration: InputDecoration(
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        floatingLabelBehavior: floatingLabelBehavior,
        contentPadding: contentPadding,
        prefixText:  prefixText,
        suffixText: suffixText ,
        // labelText: labelText,
        labelStyle: getTextStyle(),
        hintStyle: getTextStyle(),
        hintText: labelText,
        errorMaxLines: 3,
        errorText: errorText.isEmpty ? null : errorText,
        prefixStyle: getTextStyle(),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        border: inputBorder,
      ),
      onSaved: onSaved,
      dialogTitle: dialogTitle,
      dialogSearchHint: dialogSearchHint,
      enableSearch: enableSearch,
    );
  }

}

Widget getWidgetWithPadding(Widget textField, {double topPadding = 20}) => Padding(
      padding:EdgeInsets.fromLTRB(40, topPadding, 40, 0),
      child: textField,
    );

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
