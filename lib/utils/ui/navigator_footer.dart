import 'package:flutter/material.dart';

class NavigatorFooter extends StatefulWidget {
  final void Function() onTap;
  final bool isActive;
  final bool isPadding;
  final bool isChecked;
  final BuildContext context;
  final String title;
  final bool isCenter;

  const NavigatorFooter({
    @required this.title,
    Key key,
    this.onTap,
    this.isActive = true,
    this.isPadding = true,
    this.isChecked = false,
    this.isCenter = true,
    this.context,
  }) : super(key: key);

  @override
  _NavigatorFooterState createState() => _NavigatorFooterState();
}

class _NavigatorFooterState extends State<NavigatorFooter> {
  final _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: _getNextButton(_width),
    );
  }

  Widget _getNextButton(_width) {
    return GestureDetector(
      onTap: widget.isActive
          ? widget.onTap
          : () {
              _focusNode.requestFocus(new FocusNode());
            },
      child: Padding(
        padding: widget.isPadding
            ? EdgeInsets.fromLTRB(
                _width < 400 ? 10 : 20, 20, _width < 400 ? 10 : 20, 10)
            : EdgeInsets.zero,
        child: Align(
          alignment: widget.isCenter ? Alignment.center : Alignment.bottomLeft,
          child: SizedBox(
            width: _width * 0.3,
            child: RaisedButton(
              onPressed: widget.isActive
                  ? widget.onTap
                  : () {
                      _focusNode.requestFocus(new FocusNode());
                    },
              color: Theme.of(context).primaryColor,
              child: Text(
                widget.title.toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: _width * 0.04,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
