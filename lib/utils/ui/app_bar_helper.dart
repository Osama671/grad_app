import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:grad_app3/screens/wrapper.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:statusbar_util/statusbar_util.dart';

class AppBarHelper {
  static var shared = AppBarHelper();

  Widget homeAppBarWithSettings(context, title) {
    StatusbarUtil.setTranslucent();
    StatusbarUtil.setStatusBarFont(FontStyle.white);

    return AppBarHelper.shared.createAppBar(
      context: context,
      title: title,
      isHideHome: true,
      actions: [
        new IconButton(
          onPressed: () {

          },
          icon: Icon(Icons.settings),
        ),
      ],
    );
  }

  Widget createAppBar({
    BuildContext context,
    String title,
    Color backgroundColor,
    List<Widget> actions,
    double elevation = 0.0,
    bool isHideHome = false,
    Widget leading,
  }) {
    StatusbarUtil.setTranslucent();
    StatusbarUtil.setStatusBarFont(FontStyle.white);
    return GradientAppBar(
      leading: leading,
      elevation: elevation,
      actions: actions != null
          ? actions
          : [
        if (!isHideHome)
          Container(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            child: SizedBox(
              height: 27,
              child: InkWell(
                child: Image.asset(
                  'assets/home.png',
                  color: Colors.white,
                  width: 25,
                ),
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  SystemChannels.textInput.invokeMethod('TextInput.hide');

                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => Wrapper()), (
                      route) => false);
                },
              ),
            ),
          ),
      ],
      centerTitle: true,
      title: Text(
        title.toString().replaceAll('\n', ' '),
        textAlign: TextAlign.center,
        style: TextStyle(
          height: 1,
          fontSize: MediaQuery
              .of(context)
              .size
              .width * 0.04,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }
}
