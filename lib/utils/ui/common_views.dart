import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grad_app3/utils/ui/textfield_helper.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class CommonViews {
  Widget searchField({
    BuildContext context,
    Function search,
    LinearGradient gradient,
    TextEditingController searchController,
    FocusNode node,
  }) =>
      Padding(
        padding: EdgeInsets.fromLTRB(40, 20, 40, 0),
        child: TextFieldHelper.shared.createTextField(
            context: context,
            inputAction: TextInputAction.done,
            controller: searchController,
            focusNode: node,
            onChanged: (value) {
              search(value);
            },
            onSubmitted: (val) {
              FocusScope.of(context).requestFocus(new FocusNode());
              SystemChannels.textInput.invokeMethod('TextInput.hide');
            },
            labelText: 'Search',
            borderGradient: gradient,
            iconPrefixGradient: gradient,
            prefixIcon: Icon(
              Icons.search,
              color: Theme.of(context).primaryColor,
            ),
            inputFormatters: [
              //FilteringTextInputFormatter(new RegExp(' '), allow: false),
            ]),
      );

  Widget gradiantLine({double width = 200, double height = 2,LinearGradient gradient}) => SizedBox(
        width: width,
        height: height,
        child: Container(
          decoration: BoxDecoration(
            gradient: gradient,
          ),
        ),
      );

  Widget menuActionBtn({
    BuildContext context,
    String img,
    String title,
    String desc,
    double imgWidth = 33,
    double imgHeight = 33,
    void Function() onTap,
  }) {
    var _width = MediaQuery.of(context).size.width;
    var _allPading = EdgeInsets.fromLTRB(_width * 0.05, 10, _width * 0.05, 10);
    var _height = MediaQuery.of(context).size.height;
    return Padding(
      padding: _allPading,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          constraints: new BoxConstraints(
            minHeight: 50.0,
          ),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          alignment: Alignment.bottomRight,
          child: Row(
            children: <Widget>[
              new Image.asset(
                img,
                width: imgWidth,
                height: imgHeight,
                color: Theme.of(context).primaryColor,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10,
                    _height < 810 ? _height * 0.004 : _height * 0.01, 10, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      title,
                      style: TextStyle(
                          color: Colors.yellow,
                          fontSize: _width * 0.045,
                          height: 1,
                          fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 1),
                      child: SizedBox(
                        width: _width * 0.7,
                        child: Text(
                          desc.toString(),
                          maxLines: 10,
                          softWrap: false,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.yellow,
                              fontSize: _width < 370
                                  ? _width * 0.032
                                  : _width * 0.036,
                              height: 1.5,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getLoadedBut({
    BuildContext context,
    String title = '',
    void Function() onPressed,
    double leftRightPadding = 10.0,
    double height = 40,
    double bottomPadding = 50,
    RoundedLoadingButtonController buttonController,
    double topPadding = 10,
    double fontSize = 17,
    Color textColor = Colors.white,
    bool isActive = true,
    double width = 300.0,
    EdgeInsetsGeometry padding,
  }) {
    if (padding == null)
      padding = EdgeInsets.fromLTRB(
        leftRightPadding,
        topPadding,
        leftRightPadding,
        bottomPadding,
      );
    return AbsorbPointer(
      absorbing: !isActive,
      child: Container(
        child: SizedBox(
          height: height,
          child: Center(
            child: RoundedLoadingButton(
              color: Theme.of(context).primaryColor,
              controller: buttonController,
              onPressed: onPressed,
              child: Text(
                title,
                style: TextStyle(color: textColor),
              ),
            ),
          ),
          width: width,
        ),
        padding: padding,
      ),
    );
  }

  Widget hintTextView(String title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
      child: Text(
        title.toString(),
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
      ),
    );
  }
}
