import 'package:flutter/material.dart';

class UiUtiles {
  static var shared = UiUtiles();

  Widget getWrappedColumn({
    List<Widget> children,
    double spacing = 0,
    double runSpacing = 0,
  }) {
    return Wrap(
      children: children == null ? [] : children,
      spacing: spacing,
      runSpacing: runSpacing,
    );
  }

  Widget getPaddedText({
    String title,
    TextStyle style,
    double padding = 20,
    int maxLines = 5,
  }) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: Container(
        height: 37,
        child: _getText(
          title: title,
          style: style,
          maxLines: maxLines,
        ),
      ),
    );
  }

  Text _getText({String title, TextStyle style, int maxLines = 5}) {
    if (title == null) {
      title = 'null';
    }
    return Text(
      title,
      style: style,
      overflow: TextOverflow.ellipsis,
      maxLines: maxLines,
      softWrap: true,
    );
  }

  TextStyle amounTextStyle({
    Color color = Colors.black38,
    FontWeight fontWeight = FontWeight.bold,
    double fontSize = 18,
  }) {
    return TextStyle(
        color: Colors.yellow,
        fontWeight: FontWeight.bold,
        fontSize: fontSize);
  }

  TextStyle publicTextStyle({
    Color color = Colors.black38,
    FontWeight fontWeight = FontWeight.bold,
    double fontSize = -1,
    double height = 1.3,
  }) {
    if (fontSize == -1) {
      fontSize = 14;
    }
    return TextStyle(
      fontSize: fontSize,
      color: color,
      height: height,
      fontWeight: fontWeight,
    );
  }
}
