import 'package:flutter/material.dart';
import 'package:grad_app3/utils/helpers/url_helper.dart';
import 'package:vector_math/vector_math.dart' as math;

import 'custom_alert/alert.dart';
import 'custom_alert/dialog_button.dart';



typedef void AlertHelperCompletionHandler();
typedef void ConfirmationAlertHelperCompletionHandler(bool isPositive);

class AlertHelper {
  static var shared = AlertHelper();
  String _contactUsPhone = '0790308976';

  BuildContext context;

  Future<void> showCustomDialog({
    String submitLabel = '',
    String cancelButtonLabel = '',
    @required BuildContext privateContext,
    Widget body,
    Widget icon,
    AlertHelperCompletionHandler cancelCompletionHandler,
    AlertHelperCompletionHandler completionHandler,
  }) async {
    if (cancelButtonLabel.isEmpty) {
      cancelButtonLabel = 'Cancel';
    }
    return Alert(
      context: privateContext,
      title: icon,
      closeIcon:
      Icon(Icons.close, color: Theme.of(privateContext).primaryColor),
      closeFunction: () {
        Navigator.of(privateContext, rootNavigator: true).pop();
      },
      image: icon,
      onWillPopActive: true,
      type: null,
      buttons: [
        DialogButton(
          color: Theme.of(privateContext).primaryColor,
          onPressed: () {
            Navigator.of(privateContext, rootNavigator: true).pop();
            if (cancelCompletionHandler != null) cancelCompletionHandler.call();

          },
          child: Text(
            cancelButtonLabel.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(privateContext).size.width * 0.04,
            ),
          ),
        ),
        DialogButton(
          onPressed: () {
            Navigator.of(privateContext, rootNavigator: true).pop();
            if (completionHandler != null) completionHandler.call();
          },
          color: Theme.of(privateContext).primaryColor,
          child: Text(
            submitLabel.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: MediaQuery.of(privateContext).size.width * 0.04,
            ),
          ),
        ),
      ],
      content: body,
    ).show();
  }


  void showAlertDialog({
    @required String message,
    String buttonLabel = '',
    String cancelButtonLabel = '',
    @required BuildContext context,
    AlertType alertType = AlertType.INFO,
    bool isCancelButton = false,
    AlertHelperCompletionHandler cancelCompletionHandler,
    bool isCallButton,
    AlertHelperCompletionHandler completionHandler,
  }) {
    if (message == null || context == null) {
      return;
    }

    if (buttonLabel.isEmpty) {
      buttonLabel = 'OK';
    }

    this.context = context;
    showAnimatedPopup(
      context: context,
      msgBody: message,
      isCancelButton: isCancelButton,
      alertType: alertType,
      cancelCompletionHandler: cancelCompletionHandler,
      submitButLabel: buttonLabel,
      cancelButtonLabel: cancelButtonLabel,
      completionHandler: completionHandler,
      isCallButton: isCallButton,
    );
  }

  Future<void> showAnimatedPopup({
    BuildContext context,
    AlertType alertType,
    bool isCallButton,
    bool isCancelButton = false,
    String submitButLabel = '',
    String cancelButtonLabel = '',
    String msgBody = '',
    AlertHelperCompletionHandler cancelCompletionHandler,
    AlertHelperCompletionHandler completionHandler,
  }) async {
    if (cancelButtonLabel.isEmpty) {
      cancelButtonLabel = 'Cancel';
    }

    bool isCall = msgBody.toLowerCase() ==
        'Please Contact us';

    if (isCallButton != null) {
      isCall = isCallButton;
    }

    return await AnimatedDialogBox.showScaleAlertBox(
      context: AlertHelper.shared.context,
      firstButton: Row(
        children: [
          if (isCall || isCancelButton)
            _getAlertButton(
              completionHandler:
                  isCancelButton ? cancelCompletionHandler : completionHandler,
              label: isCancelButton
                  ? cancelButtonLabel
                  : 'Call',
              isCancelButton: isCancelButton,
              isFirstButton: true,
            ),
          _getAlertButton(
            completionHandler: completionHandler,
            label: submitButLabel,
          ),
        ],
      ),
      icon: Container(),
      yourWidget: Container(
        child: Text(
          msgBody.toString(),
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.width * 0.045,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget _getAlertButton({
    @required AlertHelperCompletionHandler completionHandler,
    @required String label,
    bool isFirstButton = false,
    bool isCancelButton = false,
  }) =>
      new ConstrainedBox(
        constraints: new BoxConstraints(
          minWidth: MediaQuery.of(context).size.width * 0.2,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: RaisedButton(
            onPressed: () {
              Navigator.of(context, rootNavigator: true).pop();

              if (isFirstButton) {
                if (isCancelButton) {
                  if (completionHandler != null) completionHandler();
                } else {
                  UrlHelper.shared.hitLink(
                      url: 'tel:' + _contactUsPhone.replaceAll(' ', ''));
                  if (completionHandler != null) {
                    completionHandler();
                  }
                }
              } else {
                if (completionHandler != null) {
                  completionHandler();
                }
              }
            },
            color: Theme.of(context).primaryColor,
            child: Text(
              label.toString(),
              style: TextStyle(
                color: Colors.white,
                fontSize: MediaQuery.of(context).size.width * 0.04,
              ),
            ),
          ),
        ),
      );

  String getAlertAssetByAlertType(AlertType alertType) {
    if (alertType == AlertType.WARNING) return 'assets/warningIcon2.json';
    if (alertType == AlertType.ERROR) return 'assets/warningIcon.json';
    if (alertType == AlertType.SUCCESS) return 'assets/successIcon.json';
    return null;
  }

}

enum AlertType { WARNING, ERROR, SUCCESS, INFO }

class AnimatedDialogBox {
  static Future showCustomAlertBox({
    @required BuildContext context,
    @required Widget yourWidget,
    @required Widget firstButton,
  }) {
    assert(context != null, 'context is null!!');
    assert(yourWidget != null, 'yourWidget is null!!');
    assert(firstButton != null, 'Button is null!!');
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                yourWidget,
              ],
            ),
            actions: <Widget>[firstButton],
            elevation: 10,
          );
        });
  }

  static Future showScaleAlertBox({
    @required BuildContext context,
    @required Widget yourWidget,
    Widget icon,
    Widget title,
    @required Widget firstButton,
    Widget secondButton,
  }) {
    assert(context != null, 'context is null!!');
    assert(yourWidget != null, 'yourWidget is null!!');
    assert(firstButton != null, 'button is null!!');
    return showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.7),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: AlertDialog(
                shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                title: title,
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    icon,
                    Container(
                      height: 10,
                    ),
                    yourWidget
                  ],
                ),
                actions: <Widget>[
                  firstButton,
                  secondButton,
                ],
              ),
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: false,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }

  static Future showInOutDailog({
    @required BuildContext context,
    @required Widget yourWidget,
    Widget icon,
    Widget title,
    @required Widget firstButton,
    Widget secondButton,
  }) {
    assert(context != null, 'context is null!!');
    assert(yourWidget != null, 'yourWidget is null!!');
    assert(firstButton != null, 'button is null!!');
    return showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.7),
        transitionBuilder: (context, a1, a2, widget) {
          final curvedValue = Curves.fastOutSlowIn.transform(a1.value) - 1.0;
          return Transform(
            transform: Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
            child: Opacity(
              opacity: a1.value,
              child: AlertDialog(
                shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(16.0)),
                title: title,
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    icon,
                    Container(
                      height: 10,
                    ),
                    yourWidget
                  ],
                ),
                actions: <Widget>[firstButton, secondButton],
              ),
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Container();
        });
  }

  static Future showRotatedAlert({
    @required BuildContext context,
    @required Widget yourWidget,
    Widget icon,
    Widget title,
    @required Widget firstButton,
    Widget secondButton,
  }) {
    assert(context != null, 'context is null!!');
    assert(yourWidget != null, 'yourWidget is null!!');
    assert(firstButton != null, 'button is null!!');
    return showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.7),
        barrierLabel: '',
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.rotate(
            angle: math.radians(anim1.value * 360),
            child: AlertDialog(
              shape:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
              title: title,
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  icon,
                  Container(
                    height: 10,
                  ),
                  yourWidget
                ],
              ),
              actions: <Widget>[firstButton, secondButton],
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 300));
  }
}
