import 'package:flutter/material.dart';
// import 'package:map_launcher/map_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlHelper {
  static var shared = UrlHelper();

  // Future<void> openMaps({
  //   String latitude = '0',
  //   String longitude = '0',
  //   String title = 'MEPS National Wallet',
  // }) async {
  //   final availableMaps = await MapLauncher.installedMaps;

  //   double lat = 0;
  //   double lon = 0;

  //   try {
  //     lat = double.tryParse(latitude);
  //     lon = double.tryParse(longitude);
  //     if (lat == null) {
  //       lat = 0;
  //     }

  //     if (lon == null) {
  //       lon = 0;
  //     }
  //   } catch (e) {
  //     lat = 0;
  //     lon = 0;
  //   }

  //   final coords = Coords(lat, lon);

  //   if (await MapLauncher.isMapAvailable(MapType.google)) {
  //     await MapLauncher.showMarker(
  //       mapType: MapType.google,
  //       coords: coords,
  //       title: title,
  //     );
  //   } else {
  //     await availableMaps.first.showMarker(
  //       coords: Coords(lat, lon),
  //       title: title,
  //     );
  //   }
  // }

  hitLink({@required String url}) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
